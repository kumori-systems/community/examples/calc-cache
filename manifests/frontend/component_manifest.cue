package component

import k "kumori.systems/kumori:kumori"

#Artifact: {
  ref: name:  "frontend"

  description: {

    srv: {
      server: {
        entrypoint: { protocol: "http", port: 8080 }
      }
      client: {
        restapiclient: { protocol: "http" }
      }
    }

    config: {
      parameter: {
        appconfig: {
          param_one: string | *"default_param_one"
          param_two: number | *123
        }
        calculatorEnv: string | *"default_calculatorEnv"
        restapiclientPortEnv: string | *"80"
      }
      resource: {
        secretEnv: k.#Secret
        secretJson: k.#Secret
      }
    }
    
    size: {
      bandwidth: { size: 15, unit: "M" }
    }

    probe: frontend: {
      liveness: {
        protocol: http : { port: srv.server.entrypoint.port, path: "/health" }
        startupGraceWindow: {
          unit: "ms",
          duration: 20000,
          probe: true
        }
        frequency: "medium"
        timeout: 30000  // msec
      }
      readiness: {
        protocol: http : { port: srv.server.entrypoint.port, path: "/health" }
        frequency: "medium"
        timeout: 30000 // msec
      }
    }

    code: frontend: {
      name: "frontend"
      image: {
        hub: { name: "", secret: "" }
        tag: "kumoripublic/examples-calc-cache-frontend:v3.0.8"
      }      
      mapping: {
        filesystem: {
          "/config/config.json": {
            data: value: description.config.parameter.appconfig
            format: "json"
          }
          "/config/secret.json": {
            data: secret: "secretJson"
          }
        }
        env: {
          CALCULATOR_ENV: value: description.config.parameter.calculatorEnv
          SECRET_ENV: secret: "secretEnv"
          RESTAPICLIENT_PORT_ENV: value: description.config.parameter.restapiclientPortEnv
          SERVER_PORT_ENV: value: "\(srv.server.entrypoint.port)"
        }
      }
      size: {
        memory: { size: 100, unit: "M" }
        mincpu: 100
        cpu: { size: 200, unit: "m" }
      }
    }

  }
}
