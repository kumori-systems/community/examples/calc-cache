package component

#Artifact: {
  ref: name:  "worker"

  description: {

    srv: {
      server: {
        restapiserver: { protocol: "http", port: 8080 }
      }
      duplex: {
        hello: { protocol: "tcp", port: 8081 }
      }
    }

    config: {
      parameter: {
        simulateSlowStartSec: number | *10
      }
      resource: {}
    }
    
    size: {
      bandwidth: { size: 15, unit: "M" }
    }

    probe: worker: {
      liveness: {
        protocol: http : { port: srv.server.restapiserver.port, path: "/health" }
        startupGraceWindow: {
          unit: "ms",
          duration: 20000,
          probe: true
        }
        frequency: "medium"
        timeout: 30000  // msec
      }
      readiness: {
        protocol: http : { port: srv.server.restapiserver.port, path: "/health" }
        frequency: "medium"
        timeout: 30000 // msec
      }
    }

    code: worker: {
      name: "worker"
      image: {
        hub: { name: "", secret: "" }
        tag: "kumoripublic/examples-calc-cache-worker:v3.0.8"
      }
      mapping: {
        filesystem: {}
        env: {
          RESTAPISERVER_PORT_ENV: value: "\(srv.server.restapiserver.port)"
          HELLOSERVER_PORT_ENV: value: "\(srv.duplex.hello.port)"
          SIMULATE_SLOWSTART_SEC: value: "\(description.config.parameter.simulateSlowStartSec)"
        }
      }
      size: {
        memory: { size: 100, unit: "M" }
        mincpu: 100
        cpu: { size: 200, unit: "m" }
      }
    }

  }
}
