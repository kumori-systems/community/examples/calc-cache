package kmodule

{
	local:  true
	domain: "kumori.examples"
	module: "calccache"
	cue:    "0.4.2"
	version: [
		3,
		0,
		10,
	]
	dependencies: "kumori.systems/kumori": {
		target: "kumori.systems/kumori/@1.1.6"
		query:  "1.1.6"
	}
	sums: "kumori.systems/kumori/@1.1.6": "jsXEYdYtlen2UgwDYbUCGWULqQIigC6HmkexXkyp/Mo="
	spec: [
		1,
		0,
	]
}
