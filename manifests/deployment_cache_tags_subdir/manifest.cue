package deployment
// If a package name other than "deployment" is used, then the
// "kumorictl register deployment" operation must include the "--package" flag.

import (
  s ".../service_cache_tags:service"
)

#Deployment: {
  name: "calc_dep"
  artifact: s.#Artifact
  config: {
    parameter: particular
    resource: {
      secretEnv: secret: "calcenv"
      secretJson: secret: "calcjson"
    }
    scale: detail: {
      frontend: hsize: 1
      cache: hsize: 1
      worker: hsize: 3
    }
    resilience: 0
  }
}
