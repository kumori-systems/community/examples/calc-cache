package service

import (
  k "kumori.systems/kumori:kumori"
  f ".../frontend:component"
  c ".../cache:component"
  w ".../worker:component"
)

#Artifact: {
  ref: name:  "service_cache_tags"

  description: {
    config: {
      parameter: {
        appconfig: {
          param_one : string
          param_two : number
        }
        calculatorEnv: string
        restapiclientPortEnv: string
        simulateSlowWorkerStartSec: number
      }
      resource: {
        secretEnv: k.#Secret
        secretJson: k.#Secret
      }      
    }

    role: {
      frontend: {
        artifact: f.#Artifact
        config: {
          parameter: {
            appconfig: description.config.parameter.appconfig
            calculatorEnv: description.config.parameter.calculatorEnv
            restapiclientPortEnv: description.config.parameter.restapiclientPortEnv
          }
          resource: {
            secretEnv: description.config.resource.secretEnv
            secretJson: description.config.resource.secretJson
          }
          resilience: description.config.resilience
        }
      }
      cache: {
        artifact: c.#Artifact
        config: {
          parameter: {
            restapiclientPortEnv: description.config.parameter.restapiclientPortEnv
          }
          resource: {}
          resilience: description.config.resilience
        }
      }
      worker: {
        artifact: w.#Artifact
        config: {
          parameter: {
            simulateSlowStartSec: description.config.parameter.simulateSlowWorkerStartSec
          }
          resource: {}
          resilience: description.config.resilience
        }
      }      
    }

    vset: heterogeneous: {
      srv: {
        server: restapiserver: _
      }
      roles: {
        cache: meta: { usecache: true }
        worker: meta: { usecache: false }
      }
    }
    
    srv: {
      server: {
        service: { protocol: "http", port: 80 }
      }
    }

    connect: {
      serviceconnector: {
        as: "lb"
			  from: self: "service"
        to: frontend: "entrypoint": _
      }
      lbconnector: {
        as: "lb"
        from: frontend: "restapiclient"
        to: heterogeneous: "restapiserver": _
      }
      lbconnector2: {
        as: "lb"
        from: cache: "restapiclient"
        to: worker: "restapiserver": _
      }
      fullconnector: {
       as: "full"
       from: {}
       to: worker: "hello": _
      }      
    }
  }
}
