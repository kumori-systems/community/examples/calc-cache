#!/bin/bash

# Cluster variables
CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
INBOUNDNAME="calcinb"
DEPLOYNAME="calcdep"
DOMAIN="calcdomain"
SERVICEURL="calc-${CLUSTERNAME}.${REFERENCEDOMAIN}"
SECRETENVNAME="calcenv"
SECRETJSONNAME="calcjson"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"

# Change if special binaries want to be used
KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

case $1 in

'refresh-dependencies')
  cd manifests
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  ${KAM_CMD} mod dependency --delete kumori.systems/kumori
  ${KAM_CMD} mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  # Just for sanitize: clean the directory containing dependencies
  rm -rf ./cue.mod
  cd ..
  ;;

'create-secrets')
  ${KAM_CTL_CMD} register secret $SECRETENVNAME --from-file ./secrets/secretenv.txt
  ${KAM_CTL_CMD} register secret $SECRETJSONNAME --from-file ./secrets/secretjson.json
  ;;

'create-domain')
  ${KAM_CTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ;;

'deploy-inbound')
  ${KAM_CTL_CMD} register inbound $INBOUNDNAME \
    --domain $DOMAIN \
    --cert $CLUSTERCERT
  ;;

'dry-run')
  ${KAM_CMD} process deployment_nocache -t ./manifests
  ;;

'deploy-service')
  ${KAM_CMD} service deploy -d deployment_nocache -t ./manifests $DEPLOYNAME -- \
    --comment "Only frontend and worker" \
    --wait 5m
  ;;

'link')
  ${KAM_CTL_CMD} link $DEPLOYNAME:service $INBOUNDNAME:inbound
  ;;

'deploy-all')
  $0 create-secrets
  $0 create-domain
  $0 deploy-inbound
  $0 deploy-service
  $0 link
  ;;

'describe')
  ${KAM_CMD} service describe $DEPLOYNAME
  echo
  echo
  ${KAM_CMD} service describe $INBOUNDNAME
  ;;

'describe-details')
  ${KAM_CMD} service describe $DEPLOYNAME -odetails
  echo
  echo
  ${KAM_CMD} service describe $INBOUNDNAME -odetails
  ;;

# Update to the basic service
# NOTE: prior to use this command, change something in the CUE manifests, for
# example the configuration
'update-nocache')
  ${KAM_CMD} service update -d deployment_nocache -t ./manifests $DEPLOYNAME -- \
    --comment "Updating hello world service" \
    --yes \
    --wait 5m
  ;;

'update-cache')
  ${KAM_CMD} service update -d deployment_cache -t ./manifests $DEPLOYNAME -- \
    --comment "Frontend, cache and worker" \
    --yes \
    --wait 5m
  ;;

'update-cache-tags')
  ${KAM_CMD} service update -d deployment_cache_tags -t ./manifests $DEPLOYNAME -- \
    --comment "Frontend, cache and worker with tags" \
    --yes \
    --wait 5m
  ;;

# Parameter $2 is the subdirectory to be used (config1, config2)
# In this case, the JSON files must be imported first as CUE files in the same
# package.
# In the future, KAM will include a command to do this in just one step.
'update-cache-tags-subdir')
  ${KAM_CMD} cue import --package deployment ./manifests/deployment_cache_tags_subdir/$2
  ${KAM_CMD} service update -d deployment_cache_tags_subdir/$2 -t ./manifests $DEPLOYNAME -- \
    --comment "Using subdir" \
    --yes \
    --wait 5m
  ;;

# Test the calculator
'test')
  echo "---------------------------------------------------------"
  echo "Getting configuration:"
  curl https://${SERVICEURL}/getenv
  curl https://${SERVICEURL}/file/config.json
  echo
  echo "---------------------------------------------------------"
  echo "Testing calculator with no usecache header :"
  curl -d '{"expr":"1+2+3"}' -H "Content-Type: text/plain" -X POST https://${SERVICEURL}/calc
  curl -d '{"expr":"1+2+3"}' -H "Content-Type: text/plain" -X POST https://${SERVICEURL}/calc
  echo
  echo "Testing calculator with header usecache=true :"
  curl -d '{"expr":"1+2+3"}' -H "Content-Type: text/plain" -H "usecache: true" -X POST https://${SERVICEURL}/calc
  curl -d '{"expr":"1+2+3"}' -H "Content-Type: text/plain" -H "usecache: true" -X POST https://${SERVICEURL}/calc
  echo
  echo "Testing calculator with header usecache=false :"
  curl -d '{"expr":"1+2+3"}' -H "Content-Type: text/plain" -H "usecache: false" -X POST https://${SERVICEURL}/calc
  curl -d '{"expr":"1+2+3"}' -H "Content-Type: text/plain" -H "usecache: false" -X POST https://${SERVICEURL}/calc
  echo
  echo "---------------------------------------------------------"
  echo "Testing duplex and full connector:"
  curl https://${SERVICEURL}/hello
  echo
  echo "---------------------------------------------------------"
  ;;

# Undeploy service
'undeploy-service')
  ${KAM_CMD} service undeploy $DEPLOYNAME -- --wait 5m --force
  ;;

# Undeploy inbound (http and tcp)
'undeploy-inbound')
  ${KAM_CMD} service undeploy $INBOUNDNAME -- --wait 5m
  ;;

'delete-secrets')
  ${KAM_CTL_CMD} unregister secret $SECRETENVNAME
  ${KAM_CTL_CMD} unregister secret $SECRETJSONNAME
    ;;

'delete-domain')
  ${KAM_CTL_CMD} unregister domain $DOMAIN
  ;;

'undeploy-all')
  $0 undeploy-service
  $0 undeploy-inbound
  $0 delete-secrets
  $0 delete-domain
  ;;

*)
  echo "This script doesn't contain that command"
	;;

esac
