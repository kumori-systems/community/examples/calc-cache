package restapi

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

var httpClient *http.Client

func init() {
	tr := &http.Transport{
		DisableKeepAlives: true,
	}
	httpClient = &http.Client{Transport: tr}
}

type Operation struct {
	Expr       string `json:"expr"`
	Result     string `json:"res,omitempty"`
	FrontEndID string `json:"frontendID,omitempty"`
	WorkerID   string `json:"workerID,omitempty"`
	WorkerRole string `json:"workerRole,omitempty"`
}

type RestAPI struct {
	router                      *mux.Router
	httpserver                  *http.Server
	bindPort                    int
	restApiClientConnectAddress string
	InstanceID                  string
	Role                        string
	cache                       []Operation
}

func NewRestAPI(
	instanceID string, role string, bindPort int,
	restApiClientUrl string, restApiClientPort int,
) (
	r *RestAPI,
) {
	router := mux.NewRouter()
	httpserver := &http.Server{
		Addr:    ":" + strconv.Itoa(bindPort),
		Handler: router,
	}
	restApiClientConnectAddress :=
		"http://" + restApiClientUrl + ":" + strconv.Itoa(restApiClientPort)
	r = &RestAPI{
		router:                      router,
		httpserver:                  httpserver,
		bindPort:                    bindPort,
		restApiClientConnectAddress: restApiClientConnectAddress,
		InstanceID:                  instanceID,
		Role:                        role,
		cache:                       []Operation{},
	}
	router.HandleFunc("/calc", r.PostHandlerCalc).Methods("POST")
	router.HandleFunc("/hello", r.GetHandlerHello).Methods("GET")
	router.HandleFunc("/health", r.GetHandlerHealth).Methods("GET")
	return
}

func (r *RestAPI) Start() {
	fmt.Println("RestAPI.Start()")
	go func() {
		r.httpserver.ListenAndServe()
	}()
	return
}

func (r *RestAPI) Stop() {
	fmt.Println("RestAPI.Stop() Stopping")
	ctx, cancel := context.WithTimeout(
		context.Background(),
		time.Second*5,
	)
	defer cancel()
	r.httpserver.Shutdown(ctx)
	fmt.Println("RestAPI.Stop() Stopped")
	return
}

func (r *RestAPI) PostHandlerCalc(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.PostHandlerCalc()")
	res.Header().Set("Content-Type", "application/json")
	var op Operation
	err := json.NewDecoder(req.Body).Decode(&op)
	if err != nil {
		fmt.Println("RestApi.PostHandlerCalc() ERROR", err.Error())
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	fmt.Println("RestApi.PostHandlerCalc() op=", op)
	var opResult Operation
	opResult, err = r.doOperation(op)
	if err != nil {
		fmt.Println("RestApi.PostHandlerCalc() ERROR", err.Error())
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	json.NewEncoder(res).Encode(opResult)
	return
}

func (r *RestAPI) GetHandlerHello(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.GetHandlerHello()")
	responseStr, err := r.doHello()
	if err != nil {
		fmt.Println("RestApi.GetHandlerHello() ERROR", err.Error())
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	res.WriteHeader(http.StatusOK)
	fmt.Fprintf(res, responseStr)
	return
}

func (r *RestAPI) GetHandlerHealth(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.GetHandlerHealth()")
	res.WriteHeader(http.StatusOK)
	return
}

func (r *RestAPI) doHello() (responseStr string, err error) {
	resp, err := httpClient.Get(r.restApiClientConnectAddress + "/hello")
	if err != nil {
		return
	}
	defer resp.Body.Close()
	responseBytes, err := ioutil.ReadAll(resp.Body)
	responseStr = string(responseBytes)
	if err != nil {
		return
	}
	return
}

func (r *RestAPI) doOperation(op Operation) (resOp Operation, err error) {
	cacheResult, cacheFound := r.searchCache(op)
	if cacheFound {
		resOp.Expr = op.Expr
		resOp.FrontEndID = op.FrontEndID
		resOp.Result = cacheResult
		resOp.WorkerID = r.InstanceID
		resOp.WorkerRole = r.Role
		return
	}
	requestBody, err := json.Marshal(op)
	if err != nil {
		return
	}
	resp, err := httpClient.Post(
		r.restApiClientConnectAddress+"/calc",
		"application/json",
		bytes.NewBuffer(requestBody),
	)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &resOp)
	r.addCache(resOp)
	return
}

func (r *RestAPI) searchCache(op Operation) (result string, found bool) {
	found = false
	result = ""
	for _, resOp := range r.cache {
		if resOp.Expr == op.Expr {
			found = true
			result = resOp.Result
			break
		}
	}
	return
}

func (r *RestAPI) addCache(op Operation) {
	r.cache = append(r.cache, op)
}
