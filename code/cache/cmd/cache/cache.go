package main

import (
	"cache/pkg/restapi"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"
)

func main() {

	// Sleep? Why?
	time.Sleep(2 * time.Second)

	// Get configuration
	instanceID, err := getInstanceID()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}
	role, err := getRole()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}
	restAPIPort, err := getRestAPIPort()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}
	restApiClientUrl := "0.restapiclient" // Just default-tag + channel name
	restApiClientPort, err := getRestApiClientPort()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}

	// Starts the http servers
	restAPI := restapi.NewRestAPI(
		instanceID, role, restAPIPort,
		restApiClientUrl, restApiClientPort,
	)
	restAPI.Start()

	// Ctrl-c will abort the execution
	stopch := make(chan os.Signal)
	signal.Notify(stopch, os.Interrupt, syscall.SIGTERM)
	<-stopch
	os.Exit(0)
}

func getRestAPIPort() (restAPIPort int, err error) {
	restAPIPortStr := os.Getenv("RESTAPISERVER_PORT_ENV")
	restAPIPort, err = strconv.Atoi(restAPIPortStr)
	return
}

func getRestApiClientPort() (restApiClientPort int, err error) {
	restApiClientPortStr := os.Getenv("RESTAPICLIENT_PORT_ENV")
	restApiClientPort, err = strconv.Atoi(restApiClientPortStr)
	return
}

func getInstanceID() (instanceID string, err error) {
	instanceID = os.Getenv("KUMORI_INSTANCE_ID")
	if instanceID == "" {
		err = fmt.Errorf("InstanceID unknown")
	}
	return
}

func getRole() (roleID string, err error) {
	roleID = os.Getenv("KUMORI_ROLE")
	if roleID == "" {
		err = fmt.Errorf("RoleID unknown")
	}
	return
}

func getDeploymentID() (deploymentID string, err error) {
	deploymentID = os.Getenv("KUMORI_DEPLOYMENT")
	if deploymentID == "" {
		err = fmt.Errorf("DeploymentID unknown")
	}
	return
}
