package main

import (
	"fmt"
	"frontend/pkg/restapi"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

func main() {

	// Get configuration
	instanceID, err := getInstanceID()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}
	role, err := getRole()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}
	serverPort, err := getBindPortInt()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}
	restApiClientChannel := "restapiclient"
	restApiClientPort, err := getRestApiClientPort()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}

	// Starts the http server
	restAPI, err := restapi.NewRestAPI(
		instanceID, role, serverPort,
		restApiClientChannel, restApiClientPort,
	)
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}
	restAPI.Start()

	// Ctrl-c will abort the execution
	stopch := make(chan os.Signal)
	signal.Notify(stopch, os.Interrupt, syscall.SIGTERM)
	<-stopch
	os.Exit(0)
}

func getBindPortInt() (serverPort int, err error) {
	serverPortStr := os.Getenv("SERVER_PORT_ENV")
	serverPort, err = strconv.Atoi(serverPortStr)
	return
}

func getRestApiClientPort() (restApiClientPort int, err error) {
	restApiClientPortStr := os.Getenv("RESTAPICLIENT_PORT_ENV")
	restApiClientPort, err = strconv.Atoi(restApiClientPortStr)
	return
}

func getInstanceID() (instanceID string, err error) {
	instanceID = os.Getenv("KUMORI_INSTANCE_ID")
	if instanceID == "" {
		err = fmt.Errorf("InstanceID unknown")
	}
	return
}

func getRole() (roleID string, err error) {
	roleID = os.Getenv("KUMORI_ROLE")
	if roleID == "" {
		err = fmt.Errorf("RoleID unknown")
	}
	return
}
