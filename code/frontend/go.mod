module frontend

go 1.13

require (
	github.com/Jeffail/gabs/v2 v2.5.0
	github.com/gorilla/mux v1.7.3
)
