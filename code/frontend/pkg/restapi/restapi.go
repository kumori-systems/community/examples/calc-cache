package restapi

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/Jeffail/gabs/v2"
	"github.com/gorilla/mux"
)

const (
	filesPath    = "/config"
	kumoriConfig = "/kumori/config.json"
)

var httpClient *http.Client

func init() {
	tr := &http.Transport{
		DisableKeepAlives: true,
	}
	httpClient = &http.Client{Transport: tr}
}

type Operation struct {
	Expr       string `json:"expr"`
	Result     string `json:"res,omitempty"`
	FrontEndID string `json:"frontendID,omitempty"`
	WorkerID   string `json:"workerID,omitempty"`
	WorkerRole string `json:"workerRole,omitempty"`
}

type RestAPI struct {
	router                      *mux.Router
	httpserver                  *http.Server
	bindPort                    int
	restApiConnectAddressWorker string
	restApiConnectAddressCache  string
	restApiClientPort           int
	InstanceID                  string
	Role                        string
}

func NewRestAPI(
	instanceID string, role string, bindPort int,
	restApiClientChannel string, restApiClientPort int,
) (
	r *RestAPI, err error,
) {
	fmt.Println("NewRestAPI()")

	router := mux.NewRouter()
	httpserver := &http.Server{
		Addr:    ":" + strconv.Itoa(bindPort),
		Handler: router,
	}

	restApiConnectAddressWorker, restApiConnectAddressCache, err :=
		getConnectAddresses(restApiClientPort)
	fmt.Println("NewRestAPI() connectAddresses", restApiConnectAddressWorker, restApiConnectAddressCache)
	if err != nil {
		fmt.Println("NewRestAPI() error", err.Error())
		return
	}

	r = &RestAPI{
		router:                      router,
		httpserver:                  httpserver,
		bindPort:                    bindPort,
		restApiConnectAddressWorker: restApiConnectAddressWorker,
		restApiConnectAddressCache:  restApiConnectAddressCache,
		restApiClientPort:           restApiClientPort,
		InstanceID:                  instanceID,
		Role:                        role,
	}
	router.HandleFunc("/calc", r.PostHandlerCalc).Methods("POST")
	router.HandleFunc("/file/{filename}", r.GetHandlerReadFile).Methods("GET")
	router.HandleFunc("/getenv", r.GetHandlerGetEnv).Methods("GET")
	router.HandleFunc("/hello", r.GetHandlerHello).Methods("GET")
	router.HandleFunc("/whoareyou", r.GetHandlerWhoAreYou).Methods("GET")
	router.HandleFunc("/health", r.GetHandlerHealth).Methods("GET")
	return
}

func (r *RestAPI) Start() {
	fmt.Println("RestAPI.Start()")

	// Each 30 seconds, connect address are calculated again
	ticker := time.NewTicker(time.Second * 30)
	go func() {
		for range ticker.C {
			restApiConnectAddressWorker, restApiConnectAddressCache, err :=
				getConnectAddresses(r.restApiClientPort)
			if err != nil {
				fmt.Println("NewRestAPI() error", err.Error())
			} else if restApiConnectAddressWorker != r.restApiConnectAddressWorker ||
				restApiConnectAddressCache != r.restApiConnectAddressCache {
				fmt.Println("Start().Timer new connectAddresses", restApiConnectAddressWorker, restApiConnectAddressCache)
				r.restApiConnectAddressWorker = restApiConnectAddressWorker
				r.restApiConnectAddressCache = restApiConnectAddressCache
			}
		}
	}()

	go func() {
		r.httpserver.ListenAndServe()
	}()
	return
}

func (r *RestAPI) Stop() {
	fmt.Println("RestAPI.Stop() Stopping")
	ctx, cancel := context.WithTimeout(
		context.Background(),
		time.Second*5,
	)
	defer cancel()
	r.httpserver.Shutdown(ctx)
	fmt.Println("RestAPI.Stop() Stopped")
	return
}

func (r *RestAPI) PostHandlerCalc(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.PostHandlerCalc()")
	res.Header().Set("Content-Type", "application/json")
	useCache := false
	if req.Header.Get("usecache") == "true" {
		useCache = true
	}
	var op Operation
	err := json.NewDecoder(req.Body).Decode(&op)
	if err != nil {
		fmt.Println("RestApi.PostHandlerCalc() ERROR(1)", err.Error())
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	op.FrontEndID = r.InstanceID
	fmt.Println("RestApi.PostHandlerCalc() op=", op, ", usecache=", useCache)
	var opResult Operation
	opResult, err = r.doOperation(op, useCache)
	if err != nil {
		fmt.Println("RestApi.PostHandlerCalc() ERROR(2)", err.Error())
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	json.NewEncoder(res).Encode(opResult)
	return
}

func (r *RestAPI) GetHandlerReadFile(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.GetHandlerReadFile()")
	params := mux.Vars(req)
	fileName := filesPath + "/" + params["filename"]
	content, err := r.readFile(fileName)
	if err != nil {
		fmt.Println("RestApi.GetHandlerReadFile() ERROR", err.Error())
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	res.WriteHeader(http.StatusOK)
	fmt.Fprintf(res, content)
	return
}

func (r *RestAPI) GetHandlerGetEnv(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.GetHandlerGetEnv()")
	res.WriteHeader(http.StatusOK)
	fmt.Fprintf(res, "CALCULATOR_ENV = "+os.Getenv("CALCULATOR_ENV")+"\nSECRET_ENV = "+os.Getenv("SECRET_ENV")+"\n")
	return
}

func (r *RestAPI) GetHandlerHello(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.GetHandlerHello()")
	responseStr, err := r.doHello()
	if err != nil {
		fmt.Println("RestApi.GetHandlerHello() ERROR", err.Error())
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	res.WriteHeader(http.StatusOK)
	fmt.Fprintf(res, responseStr)
	return
}

func (r *RestAPI) GetHandlerWhoAreYou(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.GetHandlerWhoAreYou()")
	res.WriteHeader(http.StatusOK)
	fmt.Fprintf(res, "I'm "+r.InstanceID)
	return
}

func (r *RestAPI) GetHandlerHealth(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.GetHandlerHealth()")
	res.WriteHeader(http.StatusOK)
	return
}

func (r *RestAPI) doOperation(op Operation, useCache bool) (resOp Operation, err error) {
	requestBody, err := json.Marshal(op)
	if err != nil {
		return
	}
	restApiConnectAddress := r.restApiConnectAddressWorker
	if useCache {
		restApiConnectAddress = r.restApiConnectAddressCache
	}
	resp, err := httpClient.Post(
		restApiConnectAddress+"/calc",
		"application/json",
		bytes.NewBuffer(requestBody),
	)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &resOp)
	return
}

func (r *RestAPI) doHello() (responseStr string, err error) {
	resp, err := httpClient.Get(r.restApiConnectAddressWorker + "/hello")
	if err != nil {
		return
	}
	defer resp.Body.Close()
	responseBytes, err := ioutil.ReadAll(resp.Body)
	responseStr = string(responseBytes)
	if err != nil {
		return
	}
	return
}

func (r *RestAPI) readFile(filename string) (content string, err error) {
	contentByte, err := ioutil.ReadFile(filename)
	if err != nil {
		return
	}
	content = string(contentByte)
	return
}

func getConnectAddresses(
	restApiClientPort int,
) (
	restApiConnectAddressWorker string,
	restApiConnectAddressCache string,
	err error,
) {

	// /kumori/config.json Expected content:
	// {
	// 	"channels": {
	// 		"restapiclient": {
	// 			"0": {
	// 				"usecache": false
	// 			},
	// 			"1": {
	// 				"usecache": true
	// 			}
	// 		}
	// 	}
	// }

	sPort := strconv.Itoa(restApiClientPort)
	fmt.Println("RestAPI.getConnectAddresses() port=", sPort)

	content, err := ioutil.ReadFile(kumoriConfig)
	if err != nil {
		return
	}
	fmt.Println("RestAPI.getConnectAddresses() config.json=", string(content))
	jsonParsed, err := gabs.ParseJSON(content)
	if err != nil {
		return
	}

	// Note: the first "0" is the tag, the second "0" is the position in the array
	// or tag 0 (gabs json notation)
	tag0value, tag0found := getUseCache(jsonParsed.Search("channels", "restapiclient", "0"))
	fmt.Println("RestAPI.getConnectAddresses() tag0value:", tag0value, ",tag0found:", tag0found)

	// Note: the first "1" is the tag, the first "0" is the position in the array
	// or tag 1 (gabs json notation)
	tag1value, tag1found := getUseCache(jsonParsed.Search("channels", "restapiclient", "1"))
	fmt.Println("RestAPI.getConnectAddresses() tag1value:", tag1value, ",tag1found:", tag1found)

	if !tag0found && !tag1found {
		// Default value when no tags
		restApiConnectAddressCache = "http://0.restapiclient:" + sPort
		restApiConnectAddressWorker = "http://0.restapiclient:" + sPort
	} else {
		if tag0value {
			restApiConnectAddressCache = "http://0.restapiclient:" + sPort
		} else if tag1found && tag1value {
			restApiConnectAddressCache = "http://1.restapiclient:" + sPort
		} else {
			restApiConnectAddressCache = "http://notfound" // Ugly implementation!
		}

		if !tag0value {
			restApiConnectAddressWorker = "http://0.restapiclient:" + sPort
		} else if tag1found && !tag1value {
			restApiConnectAddressWorker = "http://1.restapiclient:" + sPort
		} else {
			restApiConnectAddressWorker = "http://notfound" // Ugly implementation!
		}
	}

	fmt.Println("RestAPI.getConnectAddresses()", restApiConnectAddressWorker, restApiConnectAddressCache)
	return
}

func getUseCache(tag *gabs.Container) (value bool, found bool) {

	found = false
	value = false
	for _, tagData := range tag.Children() {
		value, found = tagData.Search("user", "usecache").Data().(bool)
		if found {
			return
		}
	}

	return
}
