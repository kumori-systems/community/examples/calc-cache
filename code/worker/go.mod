module worker

go 1.13

// ORIGINAL
// require (
//   github.com/gorilla/mux v1.7.3
//   github.com/Knetic/govaluate v3.0.0
// )

require (
	github.com/Knetic/govaluate v3.0.0+incompatible
	github.com/gorilla/mux v1.7.3
)
