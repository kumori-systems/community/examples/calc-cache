package helloapi

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

type HelloAPI struct {
	router       *mux.Router
	httpserver   *http.Server
	bindPort     int
	InstanceID   string
	InstanceRole string
}

func NewHelloAPI(instanceID string, role string, bindPort int) (r *HelloAPI) {
	router := mux.NewRouter()
	httpserver := &http.Server{
		Addr:    ":" + strconv.Itoa(bindPort),
		Handler: router,
	}
	r = &HelloAPI{
		router:       router,
		httpserver:   httpserver,
		bindPort:     bindPort,
		InstanceID:   instanceID,
		InstanceRole: role,
	}
	router.HandleFunc("/hello", r.GetHandlerHello).Methods("GET")
	return
}

func (r *HelloAPI) Start() {
	fmt.Println("HelloAPI.Start()")
	go func() {
		r.httpserver.ListenAndServe()
	}()
	return
}

func (r *HelloAPI) Stop() {
	fmt.Println("HelloAPI.Stop() Stopping")
	ctx, cancel := context.WithTimeout(
		context.Background(),
		time.Second*5,
	)
	defer cancel()
	r.httpserver.Shutdown(ctx)
	fmt.Println("HelloAPI.Stop() Stopped")
	return
}

func (r *HelloAPI) GetHandlerHello(res http.ResponseWriter, req *http.Request) {
	fmt.Println("HelloAPI.GetHandlerHello()")
	res.WriteHeader(http.StatusOK)
	fmt.Fprintf(res, "Hello from "+r.InstanceID+"("+r.InstanceRole+").")
	return
}
