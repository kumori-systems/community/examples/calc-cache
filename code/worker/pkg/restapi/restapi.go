package restapi

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strconv"
	"time"

	"github.com/Knetic/govaluate"
	"github.com/gorilla/mux"
)

type Operation struct {
	Expr       string `json:"expr"`
	Result     string `json:"res,omitempty"`
	FrontEndID string `json:"frontendID,omitempty"`
	WorkerID   string `json:"workerID,omitempty"`
	WorkerRole string `json:"workerRole,omitempty"`
}

type RestAPI struct {
	router     *mux.Router
	httpserver *http.Server
	bindPort   int
	InstanceID string
	Role       string
	HelloDNS   string
}

func NewRestAPI(instanceID string, role string, bindPort int, helloDNS string) (r *RestAPI) {
	router := mux.NewRouter()
	httpserver := &http.Server{
		Addr:    ":" + strconv.Itoa(bindPort),
		Handler: router,
	}
	r = &RestAPI{
		router:     router,
		httpserver: httpserver,
		bindPort:   bindPort,
		InstanceID: instanceID,
		Role:       role,
		HelloDNS:   helloDNS,
	}
	router.HandleFunc("/calc", r.PostHandlerCalc).Methods("POST")
	router.HandleFunc("/file/{filename}", r.GetHandlerReadFile).Methods("GET")
	router.HandleFunc("/hello", r.GetHandlerHello).Methods("GET")
	router.HandleFunc("/health", r.GetHandlerHealth).Methods("GET")
	return
}

func (r *RestAPI) Start() {
	fmt.Println("RestAPI.Start()")
	go func() {
		r.httpserver.ListenAndServe()
	}()
	return
}

func (r *RestAPI) Stop() {
	fmt.Println("RestAPI.Stop() Stopping")
	ctx, cancel := context.WithTimeout(
		context.Background(),
		time.Second*5,
	)
	defer cancel()
	r.httpserver.Shutdown(ctx)
	fmt.Println("RestAPI.Stop() Stopped")
	return
}

func (r *RestAPI) PostHandlerCalc(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.PostHandlerCalc()")
	res.Header().Set("Content-Type", "application/json")
	var op Operation
	err := json.NewDecoder(req.Body).Decode(&op)
	if err != nil {
		fmt.Println("RestApi.PostHandlerCalc() ERROR", err.Error())
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	fmt.Println("RestApi.PostHandlerCalc() op=", op)

	result, err := calculate(op.Expr)
	if err != nil {
		fmt.Println("RestApi.PostHandlerCalc() ERROR", err.Error())
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	op.Result = result
	op.WorkerID = r.InstanceID
	op.WorkerRole = r.Role
	json.NewEncoder(res).Encode(op)
	return
}

func (r *RestAPI) GetHandlerReadFile(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.GetHandlerReadFile()")
	params := mux.Vars(req)
	fileName := "/config/" + params["filename"]
	content, err := r.readFile(fileName)
	if err != nil {
		fmt.Println("RestApi.GetHandlerReadFile() ERROR", err.Error())
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	res.WriteHeader(http.StatusOK)
	fmt.Fprintf(res, content)
	return
}

func (r *RestAPI) GetHandlerHello(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.GetHandlerHello()")
	endpoints, err := r.getEndpoints()
	if err != nil {
		fmt.Println("RestApi.GetHandlerHello() ERROR(1)", err.Error())
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	composedResponse := ""
	for _, endpoint := range endpoints {
		fmt.Println("RestApi.GetHandlerHello() asking to", endpoint)
		partialResponse, err := r.doHello(endpoint)
		if err != nil {
			fmt.Println("RestApi.GetHandlerHello() ERROR(2)", err.Error())
			res.WriteHeader(http.StatusBadRequest)
			return
		}
		composedResponse = composedResponse + partialResponse
	}
	res.WriteHeader(http.StatusOK)
	fmt.Fprintf(res, composedResponse)
	return
}

func (r *RestAPI) GetHandlerHealth(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.GetHandlerHealth()")
	res.WriteHeader(http.StatusOK)
	return
}

func (r *RestAPI) readFile(filename string) (content string, err error) {
	contentByte, err := ioutil.ReadFile(filename)
	if err != nil {
		return
	}
	content = string(contentByte)
	return
}

func (r *RestAPI) getEndpoints() (endpoints []string, err error) {
	endpoints = []string{}
	_, srvs, err := net.LookupSRV("", "", r.HelloDNS)
	if err != nil {
		return
	}
	for _, srv := range srvs {
		endpoint :=
			srv.Target[:len(srv.Target)-1] + // Last char is just "."
				":" +
				strconv.Itoa(int(srv.Port))
		endpoints = append(endpoints, endpoint)
	}
	return
}

func (r *RestAPI) doHello(endpoint string) (responseStr string, err error) {
	resp, err := http.Get("http://" + endpoint + "/hello")
	if err != nil {
		return
	}
	defer resp.Body.Close()
	responseBytes, err := ioutil.ReadAll(resp.Body)
	responseStr = string(responseBytes)
	if err != nil {
		return
	}
	return
}

func calculate(exprStr string) (result string, err error) {
	exp, err := govaluate.NewEvaluableExpression(exprStr)
	if err != nil {
		return
	}
	aux, err := exp.Evaluate(nil)
	if err != nil {
		return
	}
	result = fmt.Sprintf("%.2f", aux.(float64))
	return
}
