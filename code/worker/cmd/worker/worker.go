package main

import (
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"
	"worker/pkg/helloapi"
	"worker/pkg/restapi"
)

func main() {

	// Get configuration
	instanceID, err := getInstanceID()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}
	role, err := getRole()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}
	restAPIPort, err := getRestAPIPort()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}
	helloPort, err := getHelloPort()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}
	simulateSlowStartSec, err := getSimulateSlowStartSec()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}

	helloDNS := "hello"
	// helloDNS := "juanjo.test.kumori.cloud" // if without-kubernetes test

	// Simulate a slow start (liveness test purposes)
	fmt.Println("main() Waiting simulateSlowStartSec", simulateSlowStartSec)
	time.Sleep(time.Duration(simulateSlowStartSec) * time.Second)

	// Starts the http servers
	restAPI := restapi.NewRestAPI(instanceID, role, restAPIPort, helloDNS)
	restAPI.Start()
	helloAPI := helloapi.NewHelloAPI(instanceID, role, helloPort)
	helloAPI.Start()

	// Ctrl-c will abort the execution
	stopch := make(chan os.Signal)
	signal.Notify(stopch, os.Interrupt, syscall.SIGTERM)
	<-stopch
	os.Exit(0)
}

func getRestAPIPort() (restAPIPort int, err error) {
	restAPIPortStr := os.Getenv("RESTAPISERVER_PORT_ENV")
	restAPIPort, err = strconv.Atoi(restAPIPortStr)
	return
}

func getHelloPort() (helloPort int, err error) {
	helloPortStr := os.Getenv("HELLOSERVER_PORT_ENV")
	helloPort, err = strconv.Atoi(helloPortStr)
	return
}

func getSimulateSlowStartSec() (getSimulateSlowStartSec int, err error) {
	getSimulateSlowStartSecStr := os.Getenv("SIMULATE_SLOWSTART_SEC")
	getSimulateSlowStartSec, err = strconv.Atoi(getSimulateSlowStartSecStr)
	return
}

func getInstanceID() (instanceID string, err error) {
	instanceID = os.Getenv("KUMORI_INSTANCE_ID")
	if instanceID == "" {
		err = fmt.Errorf("InstanceID unknown")
	}
	return
}

func getRole() (roleID string, err error) {
	roleID = os.Getenv("KUMORI_ROLE")
	if roleID == "" {
		err = fmt.Errorf("RoleID unknown")
	}
	return
}

func getDeploymentID() (deploymentID string, err error) {
	deploymentID = os.Getenv("KUMORI_DEPLOYMENT")
	if deploymentID == "" {
		err = fmt.Errorf("DeploymentID unknown")
	}
	return
}
